Sovereign documentation
=======================
.. toctree::
   :maxdepth: 3

   about
   config
   guides
   metrics
   internals
