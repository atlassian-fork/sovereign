.. automodule:: sovereign.modifiers
    :members:
    :undoc-members:
    :show-inheritance:

Adding your own modifier
------------------------

You can use the following base class to create your own modifiers.

.. toctree::
   :maxdepth: 1

   modifiers/lib
