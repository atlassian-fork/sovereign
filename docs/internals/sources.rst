.. _sources:

Sources
=======

.. automodule:: sovereign.sources
    :members:
    :undoc-members:
    :show-inheritance:

Adding your own source
----------------------

You can use the following base class to create your own sources.

They can then be added as an entry point which will be automatically loaded by Sovereign.

.. toctree::
   :maxdepth: 1

   sources/lib

Currently implemented Sources
-----------------------------

.. toctree::
   :maxdepth: 1

   sources/file
   sources/inline
